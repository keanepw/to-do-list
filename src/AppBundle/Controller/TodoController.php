<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TodoController extends Controller
{
    /**
     * @Route("/todo", name="todo_list")
     */
    public function listAction(Request $request)
    {
        return $this->render('todo/index.html.twig');
    }
    
    /**
     * @Route("/todo/create", name="todo_create")
     */
    public function createAction(Request $request)
    {
        return $this->render('todo/index.html.twig');
    }
    
    /**
     * @Route("/todo/edit", name="todo_edit")
     */
    public function editAction(Request $request)
    {
        return $this->render('todo/index.html.twig');
    }
    
    /**
     * @Route("/todo/details", name="todo_details")
     */
    public function detailsAction(Request $request)
    {
        return $this->render('todo/index.html.twig');
    }
}
